package com.xhu.priceanalysis.service.impl;

import com.xhu.priceanalysis.mapper.CityMapper;
import com.xhu.priceanalysis.mapper.HpiMapper;
import com.xhu.priceanalysis.mapper.NewHpiMapper;
import com.xhu.priceanalysis.mapper.UserMapper;
import com.xhu.priceanalysis.pojo.City;
import com.xhu.priceanalysis.pojo.Hpi;
import com.xhu.priceanalysis.pojo.NewHpi;
import com.xhu.priceanalysis.service.CityService;
import com.xhu.priceanalysis.util.ExcepHpiValue;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tk.mybatis.mapper.entity.Example;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 王灏
 * @Date 2019/4/1 22:14
 * @Version 1.0.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ImportTest {


    @Autowired
    private CityService cityService;

    @Autowired
    private CityMapper cityMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private HpiMapper hpiMapper;
    @Autowired
    private NewHpiMapper newHpiMapper;
    @Autowired
    private ExcepHpiValue excepHpiValue;
    @Test
    public void importCity() throws IOException {
        List<City> cities = new ArrayList<>();

        File file = new File("D:/2011房价.xls");
        InputStream inputStream = new FileInputStream(file);
        HSSFWorkbook workbook = null;

        workbook = new HSSFWorkbook(inputStream);
        HSSFSheet sheet = workbook.getSheetAt(0);
        // 读取数据
        // 最后一行的行号
        int lastRowNum = sheet.getLastRowNum();
        City city = null;
        for (int i = 1; i <= lastRowNum; i++) {
            city = new City();

            city.setName(sheet.getRow(i).getCell(0).getStringCellValue().replaceAll("\t","").replaceAll(" ",""));
            cities.add(city);
        }
        cityService.SaveCities(cities);
        workbook.close();

    }
    @Test
    public void importHpi() throws IOException, ParseException {
        List<Hpi> hpis = new ArrayList<>();

        File file = new File("D:/2018房价.xls");
        InputStream inputStream = new FileInputStream(file);
        HSSFWorkbook workbook = null;

        workbook = new HSSFWorkbook(inputStream);
        HSSFSheet sheet = workbook.getSheetAt(0);
        // 读取数据
        // 最后一行的行号
        int lastRowNum = sheet.getLastRowNum();
        String year = "2018";
        String [] day = new String[]{
          year + "-01-11",
          year + "-02-11",
          year + "-03-11",
          year + "-04-11",
          year + "-05-11",
          year + "-06-11",
          year + "-07-11",
          year + "-08-11",
          year + "-09-11",
          year + "-10-11",
          year + "-11-11",
          year + "-12-11"
        };
        Hpi hpi = null;
        City city = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 1; i <= lastRowNum; i++) {

            city = new City();
            city.setName(sheet.getRow(i).getCell(0).getStringCellValue().replaceAll("\t","").replaceAll(" ",""));
            city = cityMapper.selectOne(city);

            for(int j = 1;j <= 12 ; j++) {
                hpi = new Hpi();
                hpi.setCid(city.getCid());
                hpi.setCname(city.getName());
                hpi.setDate(sdf.parse(day[j - 1]));
                hpi.setQoq(sheet.getRow(i).getCell(j).getNumericCellValue());
                hpis.add(hpi);
            }
        }
        int index = 0;
        HSSFSheet sheet1 = workbook.getSheetAt(1);
        for (int i = 1; i <= lastRowNum; i++) {
            for(int j = 1;j <= 6 ; j++) {
                hpis.get(index++).setYoy(sheet1.getRow(i).getCell(j).getNumericCellValue());
            }
        }
        index = 0;
        HSSFSheet sheet2 = workbook.getSheetAt(2);
        for (int i = 1; i <= lastRowNum; i++) {
            for(int j = 1;j <= 6 ; j++) {
                hpis.get(index++).setFixedbase(sheet2.getRow(i).getCell(j).getNumericCellValue());
            }
        }
        hpiMapper.insertList(hpis);
        workbook.close();

    }
    @Test
    public void importNewHpi() throws IOException, ParseException {
        List<NewHpi> newHpis = new ArrayList<>();

        File file = new File("D:/2017房价.xls");
        InputStream inputStream = new FileInputStream(file);
        HSSFWorkbook workbook = null;

        workbook = new HSSFWorkbook(inputStream);
        HSSFSheet sheet = workbook.getSheetAt(3);
        // 读取数据
        // 最后一行的行号
        int lastRowNum = sheet.getLastRowNum();
        String year = "2017";
        String [] day = new String[]{
                year + "-01-11",
                year + "-02-11",
                year + "-03-11",
                year + "-04-11",
                year + "-05-11",
                year + "-06-11",
                year + "-07-11",
                year + "-08-11",
                year + "-09-11",
                year + "-10-11",
                year + "-11-11",
                year + "-12-11"
        };
        NewHpi newHpi = null;
        City city = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 1; i <= lastRowNum; i++) {

            city = new City();
            city.setName(sheet.getRow(i).getCell(0).getStringCellValue().replaceAll("\t","").replaceAll(" ",""));
            city = cityMapper.selectOne(city);

            List<NewHpi> cityHpi = new ArrayList<>();
            for(int j = 1;j <= 12 ; j++) {
                newHpi = new NewHpi();
                newHpi.setCid(city.getCid());
                newHpi.setCname(city.getName());
                newHpi.setDate(sdf.parse(day[j - 1]));
                newHpi.setType(1);
                newHpi.setIndexValue(sheet.getRow(i).getCell(j).getNumericCellValue());
                cityHpi.add(newHpi);
            }
//            ExcepHpiValue.calExcepValue(cityHpi);
            int insertList = newHpiMapper.insertList(cityHpi);
        }

        HSSFSheet sheet1 = workbook.getSheetAt(4);
        for (int i = 1; i <= lastRowNum; i++) {
            city = new City();
            city.setName(sheet1.getRow(i).getCell(0).getStringCellValue().replaceAll("\t","").replaceAll(" ",""));
            city = cityMapper.selectOne(city);

            List<NewHpi> cityHpi = new ArrayList<>();
            for(int j = 1;j <= 12 ; j++) {
                newHpi = new NewHpi();
                newHpi.setCid(city.getCid());
                newHpi.setCname(city.getName());
                newHpi.setDate(sdf.parse(day[j - 1]));
                newHpi.setType(2);
                newHpi.setIndexValue(sheet1.getRow(i).getCell(j).getNumericCellValue());
                cityHpi.add(newHpi);
            }
//            ExcepHpiValue.calExcepValue(cityHpi);
            int insertList = newHpiMapper.insertList(cityHpi);
        }
        HSSFSheet sheet2 = workbook.getSheetAt(5);
        for (int i = 1; i <= lastRowNum; i++) {
            city = new City();
            city.setName(sheet2.getRow(i).getCell(0).getStringCellValue().replaceAll("\t","").replaceAll(" ",""));
            city = cityMapper.selectOne(city);

            List<NewHpi> cityHpi = new ArrayList<>();
            for(int j = 1;j <= 12 ; j++) {
                newHpi = new NewHpi();
                newHpi.setCid(city.getCid());
                newHpi.setCname(city.getName());
                newHpi.setDate(sdf.parse(day[j - 1]));
                newHpi.setType(3);
                newHpi.setIndexValue(sheet2.getRow(i).getCell(j).getNumericCellValue());
                cityHpi.add(newHpi);
            }
//            ExcepHpiValue.calExcepValue(cityHpi);
            int insertList = newHpiMapper.insertList(cityHpi);
        }
        workbook.close();

    }
    @Test
    public void exceptUnsusValue() {
        int[] years = {2011,2012,2013,2014,2015,2016,2017};
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        Date endDate = null;
        for (int cid = 1; cid < 70; cid++) {
            for (int i = 0; i < years.length; i++) {
                for (int type = 1; type <= 3; type++) {
                    try {
                        startDate = sdf.parse((years[i] - 1) + "-" + "12" + "-" + "10");
                        endDate = sdf.parse((years[i] + 1)+ "-" + "01" + "-"+ "12");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Example example = new Example(NewHpi.class);
                    Example.Criteria criteria = example.createCriteria();
                    criteria.andEqualTo("cid",cid);
                    criteria.andEqualTo("type",type);
                    criteria.andBetween("date",startDate,endDate);
                    List<NewHpi> newHpis = newHpiMapper.selectByExample(example);
                    excepHpiValue.calExcepValue(newHpis);
                }


            }

        }
    }
}

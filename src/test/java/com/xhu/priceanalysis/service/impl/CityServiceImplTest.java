package com.xhu.priceanalysis.service.impl;

import com.xhu.priceanalysis.pojo.City;
import com.xhu.priceanalysis.service.CityService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CityServiceImplTest {

    @Autowired
    private CityService cityService;
    @Test
    public void saveCities() {
        List<City> cities = new ArrayList<>();
        City beijing = new City(1L, "北京");
        City shanghai = new City(2L, "上海");
        City guangzhou = new City(3L, "广州");
        City shenzhen = new City(4L, "深圳");
        cities.add(beijing);
        cities.add(shanghai);
        cities.add(guangzhou);
        cities.add(shenzhen);

        cityService.SaveCities(cities);
    }
}

package com.xhu.priceanalysis.service.impl;

import com.xhu.priceanalysis.mapper.NewHpiMapper;
import com.xhu.priceanalysis.pojo.NewHpi;
import com.xhu.priceanalysis.util.arima.ARIMA;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PredictTest {

    @Autowired
    private NewHpiMapper newHpiMapper;
    @Test
    public void getYoyAndQoqByCityName() {
        try {
        NewHpi record = new NewHpi();
        record.setCid(1L);
        record.setType(1);
        List<NewHpi> arraylist = newHpiMapper.select(record);
            double[] dataArray=new double[arraylist.size()-1];
            for(int i=0;i<arraylist.size()-1;i++)
                dataArray[i]=arraylist.get(i).getIndexValue();

            //System.out.println(arraylist.size());

            ARIMA arima=new ARIMA(dataArray);

            int []model=arima.getARIMAmodel();
            System.out.println("Best model is [p,q]="+"["+model[0]+" "+model[1]+"]");
            System.out.println("Predict value="+arima.aftDeal(arima.predictValue(model[0],model[1])));
//            System.out.println("Predict error="+(arima.aftDeal(arima.predictValue(model[0],model[1]))-arraylist.get(arraylist.size()-1))/arraylist.get(arraylist.size()-1)*100+"%");

            //	String[] str = (String[])list1.toArray(new String[0]);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally{
        }
    }
}

package com.xhu.priceanalysis.service;

import com.xhu.priceanalysis.mapper.FactorMapper;
import com.xhu.priceanalysis.mapper.NewHpiMapper;
import com.xhu.priceanalysis.pojo.Factor;
import com.xhu.priceanalysis.pojo.NewHpi;
import com.xhu.priceanalysis.util.analysis.Analysis;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 王灏
 * @Date 2019/6/4 9:35
 * @Version 1.0.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AnalysisTest {
    @Autowired
    private FactorMapper factorMapper;
    @Autowired
    private NewHpiMapper newHpiMapper;
    @Test
    public void analysisTest() {
        List<Factor> factors = factorMapper.selectAll();
        NewHpi newHpi = newHpiMapper.selectNew();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(newHpi.getDate());
        for (Factor factor : factors) {
            Analysis.resolveBaiduByFactor(newHpi.getCname(),factor.getKeyword(),calendar);
        }

    }
}

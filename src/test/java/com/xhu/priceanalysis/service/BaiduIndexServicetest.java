package com.xhu.priceanalysis.service;

import com.xhu.priceanalysis.mapper.SearchIndexMapper;
import com.xhu.priceanalysis.pojo.SearchIndex;
import com.xhu.priceanalysis.util.ExcepSearchValue;
import com.xhu.priceanalysis.util.reptile.BaiduIndex;
import com.xhu.priceanalysis.util.reptile.BaiduIndexService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 岳雅萱
 * @Date 2019/5/26 18:44
 * @Version 1.0.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BaiduIndexServicetest {
    @Autowired
    private SearchIndexMapper searchIndexMapper;
    @Autowired
    private BaiduIndexService baiduIndexService;
    @Autowired
    private ExcepSearchValue excepSearchValue;
    @Test
    public void saveBaiduIndex() {
        List<BaiduIndex> baiduIndices = baiduIndexService.reptileBaiduIndex();
        List<SearchIndex> searchIndexList = new ArrayList<>();
        SearchIndex searchIndex = null;
        for (BaiduIndex baiduIndex : baiduIndices) {
            searchIndex = new SearchIndex();
            searchIndex.setCityName(baiduIndex.getKeyword());
            searchIndex.setSearchTime(baiduIndex.getDate().getTime());
            searchIndex.setIndexValue(baiduIndex.getIndexValue());
            searchIndexList.add(searchIndex);
        }
        searchIndexMapper.insertList(searchIndexList);
    }
    @Test
    public void exceptUnsusValue() {
        String[] years = {"2011","2012","2013","2014","2015","2016","2017","2018","2019"};
        String[] mouths = {"01","02","03","04","05","06","07","08","09","10","11","12"};
        List<String> cityNames = searchIndexMapper.getCityNames();
        for (String cityName : cityNames) {
            for (int i = 0; i < years.length; i++) {
                for (int j = 0; j < mouths.length; j++) {
                    List<SearchIndex> searchIndexList = searchIndexMapper.listSearchIndexListByMouthAndCityName(cityName, years[i] + "-" + mouths[j]);
                    excepSearchValue.calExcepValue(searchIndexList);
                }
            }
        }
    }
}

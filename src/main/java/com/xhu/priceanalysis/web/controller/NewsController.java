package com.xhu.priceanalysis.web.controller;

import com.xhu.priceanalysis.pojo.News;
import com.xhu.priceanalysis.service.NewsService;
import com.xhu.priceanalysis.vo.PageResult;
import com.xhu.priceanalysis.vo.PredictVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 〈功能简述〉
 * 〈新闻控制器〉
 *
 * @author 王灏
 * @Date 2019/5/13 19:20
 * @Version 1.0.0
 */
@RestController
@RequestMapping("news")
public class NewsController {
    @Autowired
    private NewsService newsService;

    @GetMapping("all")
    public ResponseEntity<PageResult<News>> listNews(
            @RequestParam(value = "page",defaultValue = "1") Integer page,
            @RequestParam(value = "size",defaultValue = "5") Integer size,
            @RequestParam(value = "key",required = false) String key
    ) {
        return ResponseEntity.ok(newsService.listNews(page,size,key));
    }
    @GetMapping("today")
    public ResponseEntity<List<News>> getTodayNews() {
        return ResponseEntity.ok(newsService.getTodayNews());
    }

}

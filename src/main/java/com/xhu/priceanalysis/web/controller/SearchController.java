package com.xhu.priceanalysis.web.controller;

import com.xhu.priceanalysis.pojo.SearchIndex;
import com.xhu.priceanalysis.service.SearchService;
import com.xhu.priceanalysis.vo.EchartsVO;
import com.xhu.priceanalysis.vo.SearchIndexVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("search")
public class SearchController {
    @Autowired
    private SearchService searchService;

    /**
     *
     * @return
     */
    @GetMapping
    public ResponseEntity<List<EchartsVO>> getSearchIndexByCityNameAndStartAndEndDate(
            @RequestParam("name") String name,
            @RequestParam("startDate") String startDate,
            @RequestParam(value = "endDate",required = false) String endDate
                                                                  ) {
        return ResponseEntity.ok(searchService.getSearchIndexByCityNameAndStartAndEndDate(name,startDate,endDate));
    }
    /**
     * 搜索热度排行
     * @return
     */
    @GetMapping("hot")
    public ResponseEntity<List<SearchIndexVO>> getSearchListOrder(
            @RequestParam("startDate") String startDate,
            @RequestParam(value = "endDate",required = false) String endDate
    ) {
        return ResponseEntity.ok(searchService.getSearchListOrder(startDate,endDate));
    }
    /**
     * 查看对应城市异常值信息
     * @return
     */
    @GetMapping("exception")
    public ResponseEntity<List<SearchIndex>> getSearchExceptionInfo(
            @RequestParam("name") String name,
            @RequestParam("startDate") String startDate,
            @RequestParam(value = "endDate",required = false) String endDate
    ) {
        return ResponseEntity.ok(searchService.getSearchExceptionInfo(name,startDate,endDate));
    }

}

package com.xhu.priceanalysis.web.controller;

import com.xhu.priceanalysis.pojo.NewHpi;
import com.xhu.priceanalysis.service.NewHpiService;
import com.xhu.priceanalysis.vo.EchartsVO;
import com.xhu.priceanalysis.vo.PredictVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 王灏
 * @Date 2019/5/30 11:41
 * @Version 1.0.0
 */
@RestController
@RequestMapping("newhpi")
public class NewHpiController {
    @Autowired
    private NewHpiService newHpiService;

    @GetMapping("predict")
    public ResponseEntity<PredictVO> getPredict(
            @RequestParam("cid") Long cid
    ) {
        return ResponseEntity.ok(newHpiService.getPredict(cid));
    }
    /**
     * 查询近一年的同比、环比
     * @return
     */
    @GetMapping
    public ResponseEntity<List<EchartsVO>> getYoyAndQoqByCityName(
            @RequestParam("name") String name,
            @RequestParam("type") Integer type,
            @RequestParam("startDate") String startDate,
            @RequestParam(value = "endDate",required = false) String endDate
) {
        return ResponseEntity.ok(newHpiService.getYoyAndQoqByCityName(name,type,startDate,endDate));
    }
    /**
     * 查看对应城市异常值信息
     * @return
     */
    @GetMapping("exception")
    public ResponseEntity<List<NewHpi>> getNewHpiExceptionInfo(
            @RequestParam("name") String name,
            @RequestParam("type") Integer type,
            @RequestParam("startDate") String startDate,
            @RequestParam(value = "endDate",required = false) String endDate
    ) {
        return ResponseEntity.ok(newHpiService.getNewHpiExceptionInfo(name,type,startDate,endDate));
    }

}

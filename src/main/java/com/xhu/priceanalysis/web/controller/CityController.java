package com.xhu.priceanalysis.web.controller;

import com.xhu.priceanalysis.pojo.City;
import com.xhu.priceanalysis.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 〈功能简述〉
 * 〈城市控制器〉
 *
 * @author 王灏
 * @Date 2019/5/12 15:26
 * @Version 1.0.0
 */
@RestController
@RequestMapping("city")
public class CityController {
    @Autowired
    private CityService cityService;

    /**
     * 查询所有城市
     * @return
     */
    @GetMapping("all")
    public ResponseEntity<List<City>> listCities() {
        return ResponseEntity.ok(cityService.listCities());
    }
}

package com.xhu.priceanalysis.web.controller;

import com.xhu.priceanalysis.pojo.User;
import com.xhu.priceanalysis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * 用户管理控制器
 */
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 跟据id查询用户信息
     * @param id
     * @return 用户
     */
    @GetMapping("{id}")
    public ResponseEntity<User> getUserById(@PathVariable Long  id) {
        User user = userService.getUserById(id);
        return ResponseEntity.ok().body(user);

    }
    /**
     * 跟据id查询用户信息
     * @param id
     * @return 用户
     */
    @GetMapping("{username}")
    public ResponseEntity<User> getUserByUsername(@PathVariable String  username) {
        User user = userService.getUserByUsername(username);
        return ResponseEntity.ok().body(user);

    }

    /**
     * 跟据用户id删除用户
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteUserById(@PathVariable Long  id) {
        userService.deleteUserById(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    /**
     * 新增用户
     * @RequestBody注解将前端传来的json数据转换为对象
     * @param user
     * @return
     */
    @PostMapping
    public ResponseEntity<Void> saveUser(@RequestBody User user) {
        userService.saveUser(user);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
    /**
     * 更新用户
     *
     * @param user
     * @return
     */
    @PutMapping
    public ResponseEntity<Void> updateUser(@RequestBody User user) {
        userService.updateUser(user);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}

package com.xhu.priceanalysis.service;

import com.xhu.priceanalysis.pojo.NewHpi;
import com.xhu.priceanalysis.vo.EchartsVO;
import com.xhu.priceanalysis.vo.PredictVO;

import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 王灏
 * @Date 2019/5/15 23:07
 * @Version 1.0.0
 */
public interface NewHpiService {

    PredictVO getPredict(Long cid);
    List<EchartsVO> getYoyAndQoqByCityName(String name, Integer type, String startDate, String endDate);

    List<NewHpi> getNewHpiExceptionInfo(String name, Integer type, String startDate, String endDate);


}

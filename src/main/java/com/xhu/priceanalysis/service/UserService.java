package com.xhu.priceanalysis.service;


import com.xhu.priceanalysis.pojo.User;

/**
 * 用户管理UserService接口
 */
public interface UserService {
    /**
     * 跟据用户id查询用户
     * @param id
     * @return
     */
    User getUserById(Long id);

    /**
     * 跟据用户id删除用户
     * @param id
     */
    void deleteUserById(Long id);

    /**
     * 更新用户
     * @param user
     */
    void updateUser(User user);

    /**
     * 新增用户
     * @param user
     */
    void saveUser(User user);

    User getUserByUsername(String username);
}

package com.xhu.priceanalysis.service;

import com.xhu.priceanalysis.vo.EchartsVO;

import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 王灏
 * @Date 2019/5/29 19:45
 * @Version 1.0.0
 */
public interface HpiService {
    List<EchartsVO> getYoyAndQoqByCityName(String name);
}

package com.xhu.priceanalysis.service;

import com.xhu.priceanalysis.pojo.News;
import com.xhu.priceanalysis.vo.PageResult;

import java.util.List;

/**
 * 〈功能简述〉
 * 〈新闻相关业务接口〉
 *
 * @author 王灏
 * @Date 2019/4/22 16:50
 * @Version 1.0.0
 */
public interface NewsService {
    /**
     * 爬取新闻
     */
    void reptileNews();

    PageResult<News> listNews(Integer page, Integer size, String key);

    List<News> getTodayNews();

}

package com.xhu.priceanalysis.service.impl;

import com.xhu.priceanalysis.mapper.CityMapper;
import com.xhu.priceanalysis.mapper.NewHpiMapper;
import com.xhu.priceanalysis.mapper.NewsMapper;
import com.xhu.priceanalysis.pojo.*;
import com.xhu.priceanalysis.service.NewHpiService;
import com.xhu.priceanalysis.util.arima.ARIMA;
import com.xhu.priceanalysis.util.enums.ExceptionEnum;
import com.xhu.priceanalysis.util.exception.AnaException;
import com.xhu.priceanalysis.vo.EchartsVO;
import com.xhu.priceanalysis.vo.PredictVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 王灏
 * @Date 2019/5/15 23:07
 * @Version 1.0.0
 */
@Service
public class NewHpiServiceImpl implements NewHpiService {

    @Autowired
    private NewHpiService newHpiService;
    @Autowired
    private NewHpiMapper newHpiMapper;
    @Autowired
    private NewsMapper newsMapper;
    @Autowired
    private CityMapper cityMapper;
    public void calExceptionValue() {

        List<City> cities = cityMapper.selectAll();
        for (City city : cities) {
            Long cid = city.getCid();
            Hpi hpi = new Hpi();
            hpi.setCid(cid);
        }


    }

    @Override
    public PredictVO getPredict(Long cid) {
        PredictVO predictVO = new PredictVO();
        NewHpi qoqhpi = new NewHpi();
        qoqhpi.setCid(cid);
        qoqhpi.setType(1);
        List<NewHpi> qoqhpis = newHpiMapper.select(qoqhpi);
        NewHpi yoyhpi = new NewHpi();
        yoyhpi.setCid(cid);
        yoyhpi.setType(2);
        List<NewHpi> yoyhpis = newHpiMapper.select(yoyhpi);

        double[] dataArrayQoq=new double[qoqhpis.size()-1];
        double[] dataArrayYoy=new double[yoyhpis.size()-1];
        for(int i=0;i<qoqhpis.size()-1;i++) {
            dataArrayQoq[i]=qoqhpis.get(i).getIndexValue();
        }
        for(int i=0;i<yoyhpis.size()-1;i++) {
            dataArrayYoy[i]=yoyhpis.get(i).getIndexValue();
        }
       predictVO.setYoy(calPredict(dataArrayQoq));
       predictVO.setQoq(calPredict(dataArrayYoy));

        return predictVO;
    }
    private Double calPredict(double[] dataArray) {
        ARIMA arima=new ARIMA(dataArray);

        int []model=arima.getARIMAmodel();
        System.out.println("Best model is [p,q]="+"["+model[0]+" "+model[1]+"]");
        System.out.println("Predict value="+arima.aftDeal(arima.predictValue(model[0],model[1])));
        return arima.aftDeal(arima.predictValue(model[0],model[1]));
    }
    @Override
    public List<EchartsVO> getYoyAndQoqByCityName(String name, Integer type, String startDate, String endDate) {
        List<EchartsVO> yoyAndQoqByCityName = newHpiMapper.getYoyAndQoqByCityName(name,type,startDate,endDate);
        if (CollectionUtils.isEmpty(yoyAndQoqByCityName)) {
            throw new AnaException(ExceptionEnum.CITY_YOY_AND_QOQ_NOT_FOUND);
        }
        return  yoyAndQoqByCityName;
    }

    @Override
    public List<NewHpi> getNewHpiExceptionInfo(String name, Integer type, String startDate, String endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date start = null;
        Date end = null;
        try {
            start = sdf.parse(startDate);
            end = sdf.parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Example example = new Example(NewHpi.class);
        example.orderBy("date").desc();
        Example.Criteria criteria = example.createCriteria();

        criteria.andEqualTo("cname",name);
        criteria.andEqualTo("type",type);
        criteria.andEqualTo("unusualValue",true);
        criteria.andBetween("date",start,end);
        List<NewHpi> newHpis = newHpiMapper.selectByExample(example);
        for (NewHpi newHpi : newHpis) {
            News news = new News();
            news.setPublishTime(sdf.format(newHpi.getDate()));
            List<News> newsList = newsMapper.select(news);
            newHpi.setNewsList(newsList);
        }

        return newHpis;
    }


}

package com.xhu.priceanalysis.service.impl;

import com.xhu.priceanalysis.mapper.HpiMapper;
import com.xhu.priceanalysis.service.HpiService;
import com.xhu.priceanalysis.util.enums.ExceptionEnum;
import com.xhu.priceanalysis.util.exception.AnaException;
import com.xhu.priceanalysis.vo.EchartsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 王灏
 * @Date 2019/5/29 19:45
 * @Version 1.0.0
 */
@Service
public class HpiServiceImpl implements HpiService {
    @Autowired
    private HpiMapper hpiMapper;

    @Override
    public List<EchartsVO> getYoyAndQoqByCityName(String name) {
        List<EchartsVO> yoyAndQoqByCityName = hpiMapper.getYoyAndQoqByCityName(name);
        if (CollectionUtils.isEmpty(yoyAndQoqByCityName)) {
            throw new AnaException(ExceptionEnum.CITY_YOY_AND_QOQ_NOT_FOUND);
        }
        return  yoyAndQoqByCityName;
    }
}

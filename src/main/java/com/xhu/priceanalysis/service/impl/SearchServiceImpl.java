package com.xhu.priceanalysis.service.impl;

import com.xhu.priceanalysis.mapper.NewsMapper;
import com.xhu.priceanalysis.mapper.SearchIndexMapper;
import com.xhu.priceanalysis.pojo.News;
import com.xhu.priceanalysis.pojo.SearchIndex;
import com.xhu.priceanalysis.service.SearchService;
import com.xhu.priceanalysis.util.enums.ExceptionEnum;
import com.xhu.priceanalysis.util.exception.AnaException;
import com.xhu.priceanalysis.vo.EchartsVO;
import com.xhu.priceanalysis.vo.SearchIndexVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 王灏
 * @Date 2019/5/29 22:14
 * @Version 1.0.0
 */
@Service
public class SearchServiceImpl implements SearchService {
    @Autowired
    private SearchIndexMapper searchIndexMapper;
    @Autowired
    private NewsMapper newsMapper;
    @Override
    public List<EchartsVO> getSearchIndexByCityNameAndStartAndEndDate(String name, String startDate, String endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date start = null;
        Date end = null;
        List<EchartsVO> yoyAndQoqByCityName = null;
        try {
            start = sdf.parse(startDate);
            if (StringUtils.isNotBlank(endDate)) {
                end = sdf.parse(endDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (StringUtils.isNotBlank(endDate)) {
            yoyAndQoqByCityName = searchIndexMapper.getYoyAndQoqByCityNameAndStartAndEnd(name,start,end);
        } else {
            yoyAndQoqByCityName = searchIndexMapper.getYoyAndQoqByCityName(name,start);
        }

        if (CollectionUtils.isEmpty(yoyAndQoqByCityName)) {
            throw new AnaException(ExceptionEnum.SEARCHINDEX_NOT_FOUND);
        }
        return  yoyAndQoqByCityName;
    }

    @Override
    public List<SearchIndexVO> getSearchListOrder(String startDate, String endDate) {
        ;
        return searchIndexMapper.getSearchListOrder(startDate,endDate);
    }

    @Override
    public List<SearchIndex> getSearchExceptionInfo(String name, String startDate, String endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date start = null;
        Date end = null;
        try {
            start = sdf.parse(startDate);
            end = sdf.parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Example example = new Example(SearchIndex.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("cityName",name);
        criteria.andEqualTo("unusualValue",true);
        criteria.andBetween("searchTime",start,end);
        List<SearchIndex> searchIndexList = searchIndexMapper.selectByExample(example);
        for (SearchIndex searchIndex : searchIndexList) {
            News news = new News();
            news.setPublishTime(sdf.format(searchIndex.getSearchTime()));
            List<News> newsList = newsMapper.select(news);
            searchIndex.setNewsList(newsList);
        }
        return searchIndexList;
    }
}

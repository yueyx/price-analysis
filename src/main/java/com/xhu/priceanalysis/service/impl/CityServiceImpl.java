package com.xhu.priceanalysis.service.impl;

import com.xhu.priceanalysis.mapper.CityMapper;
import com.xhu.priceanalysis.pojo.City;
import com.xhu.priceanalysis.service.CityService;
import com.xhu.priceanalysis.util.enums.ExceptionEnum;
import com.xhu.priceanalysis.util.exception.AnaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author
 * @Date 2019/3/30 22:06
 * @Version 1.0.0
 */
@Service
public class CityServiceImpl implements CityService {
    @Autowired
    private CityMapper cityMapper;

    @Override
    public void SaveCities(List<City> cities) {
        int count = cityMapper.insertList(cities);
        if (count == 0) {
            throw new AnaException(ExceptionEnum.SAVE_CITY_FAILED);
        }
    }

    @Override
    public void saveCity(City city) {
        int count = cityMapper.insert(city);
        if (count == 0) {
            throw new AnaException(ExceptionEnum.SAVE_CITY_FAILED);
        }
    }

    @Override
    public List<City> listCities() throws AnaException {
        List<City> cities = cityMapper.selectAll();
        if (CollectionUtils.isEmpty(cities)) {
            throw new AnaException(ExceptionEnum.CITY_NOT_FOUND);
        }
        return cities;
    }
}

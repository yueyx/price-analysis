package com.xhu.priceanalysis.service.impl;

import com.xhu.priceanalysis.mapper.UserMapper;
import com.xhu.priceanalysis.pojo.User;
import com.xhu.priceanalysis.service.UserService;
import com.xhu.priceanalysis.util.exception.AnaException;
import com.xhu.priceanalysis.util.enums.ExceptionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户管理service实现类
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public User getUserById(Long id) {
        User user = userMapper.selectByPrimaryKey(id);
        // 如果用户未查到，则抛出异常
        if (user == null) {
            throw new AnaException(ExceptionEnum.USER_NOT_FOUND);
        }
        return user;
    }

    @Override
    public void deleteUserById(Long id) {
        int count = userMapper.deleteByPrimaryKey(id);
        // count != 1,表示执行删除操作失败，抛出异常
        if (count != 1) {
            throw new AnaException(ExceptionEnum.DELETE_USER_FAILED);
        }
    }

    @Override
    public void updateUser(User user) {
        int count = userMapper.updateByPrimaryKey(user);
        if (count != 1) {
            throw new AnaException(ExceptionEnum.UPDATE_USER_FAILED);
        }
    }

    @Override
    public void saveUser(User user) {
        // 数据库使用自增主键，设置主键为空，自动生成

        user.setUid(null);
        int count = userMapper.insert(user);
        if (count != 1) {
            throw new AnaException(ExceptionEnum.SAVE_USER_FAILED);
        }
    }

    @Override
    public User getUserByUsername(String username) {
        User user = new User();
        return null;
    }
}

package com.xhu.priceanalysis.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xhu.priceanalysis.mapper.NewsMapper;
import com.xhu.priceanalysis.pojo.News;
import com.xhu.priceanalysis.service.NewsService;
import com.xhu.priceanalysis.util.enums.ExceptionEnum;
import com.xhu.priceanalysis.util.exception.AnaException;
import com.xhu.priceanalysis.util.reptile.SogouReptile;
import com.xhu.priceanalysis.util.reptile.SogouTodayReptile;
import com.xhu.priceanalysis.vo.PageResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 王灏
 * @Date 2019/4/22 16:53
 * @Version 1.0.0
 */
@Service
public class NewsServiceImpl implements NewsService {
    @Autowired
    private NewsMapper newsMapper;

    @Override
    public void reptileNews() {
//        try {
//            //获取整个网站的根节点，也就是html开头部分一直到结束
//
//            String url = "https://news.sogou.com/news?mode=1&manual=&query=%B7%BF%BC%DB&time=0&sort=0&page=1&w=03009900&dr=1";
//            int index = 1;
//            Document document = Jsoup.connect(url).get();
//            Elements result = document.select("#main > div > div");
//            while (result != null) {
//                List<News> news = SogouReptile.reptileNews(result);
//                if (!CollectionUtils.isEmpty(news)) {
//                    try {
//                        int i = newsMapper.insertList(news);
//                    } catch (Exception e) {
//
//                    }
//
//                }
//                index = index + 1;
//                url = "https://news.sogou.com/news?mode=1&manual=&query=%B7%BF%BC%DB&time=0&sort=0&page=" + index + "&w=03009900&dr=1";
//                document = Jsoup.connect(url).get();
//                result = document.select("#main > div > div");
//            }
//
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        List<News> news = SogouReptile.reptileNews();
        if (!CollectionUtils.isEmpty(news)) {
            for (News news1 : news) {
                try {
                    newsMapper.insert(news1);
                } catch (Exception e) {
                }

            }


        }
    }

    @Override
    public PageResult<News> listNews(Integer page, Integer size, String key) {
        PageHelper.startPage(page,size);
        Example example = new Example(News.class);
        example.excludeProperties("id","publishContent");
        if (StringUtils.isNotBlank(key)) {
            example.createCriteria().orLike("title","%" + key + "%").orLike("briefIntroduction","%" + key + "%");
        }
        List<News> news = newsMapper.selectByExample(example);
        if (CollectionUtils.isEmpty(news)) {
            throw new AnaException(ExceptionEnum.NEWS_NOT_FOUND);
        }
        PageInfo<News> newsPageInfo = new PageInfo<>(news);
        return new PageResult<>(newsPageInfo.getTotal(),newsPageInfo.getPages(), news);
    }

    @Override
    public List<News> getTodayNews() {
        List<News> todayNews = SogouTodayReptile.getTodayNews();
        if (CollectionUtils.isEmpty(todayNews)) {
            throw new AnaException(ExceptionEnum.TODAY_NEWS_NOT_FOUND);
        }
        return todayNews;
    }
}

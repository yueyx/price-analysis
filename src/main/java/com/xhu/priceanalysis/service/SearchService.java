package com.xhu.priceanalysis.service;

import com.xhu.priceanalysis.pojo.SearchIndex;
import com.xhu.priceanalysis.vo.EchartsVO;
import com.xhu.priceanalysis.vo.SearchIndexVO;

import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 王灏
 * @Date 2019/5/29 22:14
 * @Version 1.0.0
 */
public interface SearchService {
    List<EchartsVO> getSearchIndexByCityNameAndStartAndEndDate(String name, String startDate, String endDate);

    List<SearchIndexVO> getSearchListOrder(String startDate, String endDate);

    List<SearchIndex> getSearchExceptionInfo(String name, String startDate, String endDate);
}

package com.xhu.priceanalysis.service;

import com.xhu.priceanalysis.pojo.City;
import com.xhu.priceanalysis.util.exception.AnaException;

import java.util.List;

/**
 * 〈功能简述〉
 * 〈城市管理接口〉
 *
 * @author
 * @Date 2019/3/30 22:05
 * @Version 1.0.0
 */
public interface CityService {

    /**
     * 批量保存城市
     * @param cities
     */
    void SaveCities(List<City> cities);

    /**
     * 保存单个城市
     * @param city
     */
    void saveCity(City city);

    /**
     * 查询所有城市
     * @return
     */
    List<City> listCities() throws AnaException;
}

package com.xhu.priceanalysis.mapper;

import com.xhu.priceanalysis.pojo.City;


/**
 * 〈功能简述〉
 * 〈城市DAO〉
 *
 * @author
 * @Date 2019/3/30 22:03
 * @Version 1.0.0
 */
public interface CityMapper extends BaseMapper<City> {

//    @Insert({
//            "<script>",
//            "INSERT INTO city(cid,name)",
//            "VALUES",
//            "<foreach collection='cities' item='item' index='index' separator=','>",
//            "(#{item.cid},#{item.name})",
//            "</foreach>",
//            "</script>"
//    })
//    int saveCities(@Param("cities") List<City> cities);
}

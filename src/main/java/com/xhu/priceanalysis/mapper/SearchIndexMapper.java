package com.xhu.priceanalysis.mapper;

import com.xhu.priceanalysis.pojo.SearchIndex;
import com.xhu.priceanalysis.vo.EchartsVO;
import com.xhu.priceanalysis.vo.SearchIndexVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 岳雅萱
 * @Date 2019/5/26 18:40
 * @Version 1.0.0
 */
public interface SearchIndexMapper extends BaseMapper<SearchIndex> {
    @Select("SELECT search_time name,index_value value1 FROM search_index\n" +
            "WHERE city_name = #{name} AND DATEDIFF(search_time,#{startDate}) >= 0 AND DATEDIFF(#{endDate},search_time) > 0 ORDER BY search_time")
    List<EchartsVO> getYoyAndQoqByCityNameAndStartAndEnd(String name, Date startDate, Date endDate);

    @Select("SELECT search_time name,index_value value1 FROM search_index\n" +
            "WHERE city_name = #{name} AND DATEDIFF(search_time,#{startDate}) >= 0 ORDER BY search_time")
    List<EchartsVO> getYoyAndQoqByCityName(String name, Date startDate);

    @Select("SELECT city_name FROM\n" +
            "search_index GROUP BY city_name")
    List<String> getCityNames();
    @Select("SELECT * FROM\n" +
            "search_index WHERE city_name = #{cityName} AND DATE_FORMAT(search_time,'%Y-%m') = #{date}\n")
    List<SearchIndex> listSearchIndexListByMouthAndCityName(@Param("cityName") String cityName,@Param("date") String date);

    @Select("SELECT city_name,SUM(index_value) total FROM\n" +
            "search_index \n" +
            "WHERE  DATEDIFF(search_time,#{startDate}) >= 0 AND DATEDIFF(#{endDate},search_time) > 0\n" +
            "GROUP BY city_name")
    List<SearchIndexVO> getSearchListOrder(String startDate, String endDate);
}

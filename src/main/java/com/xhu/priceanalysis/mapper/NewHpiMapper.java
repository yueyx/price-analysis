package com.xhu.priceanalysis.mapper;

import com.xhu.priceanalysis.pojo.NewHpi;
import com.xhu.priceanalysis.vo.EchartsVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 岳雅萱
 * @Date 2019/5/15 23:06
 * @Version 1.0.0
 */
public interface NewHpiMapper extends BaseMapper<NewHpi> {
    @Select("SELECT date name,index_value value1 FROM\n" +
            "new_hpi\n" +
            "WHERE cname = #{cname} AND type = #{type} AND DATEDIFF(date,#{startDate}) >= 0 AND DATEDIFF(#{endDate},date) > 0 ORDER BY date")
    List<EchartsVO> getYoyAndQoqByCityName(@Param("cname") String cname, @Param("type") Integer type,@Param("startDate") String startDate,@Param("endDate") String endDate);

    @Select("SELECT * FROM new_hpi\n" +
            "WHERE cid = 1 AND unusual_value = 1 ORDER BY date DESC LIMIT 1")
    NewHpi selectNew();

}

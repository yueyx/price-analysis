package com.xhu.priceanalysis.mapper;

import com.xhu.priceanalysis.pojo.User;
import tk.mybatis.mapper.common.Mapper;

/**
 * mabatisMapper文件，继承自通用mapper
 */
public interface UserMapper extends Mapper<User> {
}

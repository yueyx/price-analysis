package com.xhu.priceanalysis.mapper;

import com.xhu.priceanalysis.pojo.News;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 〈功能简述〉
 * 〈新闻DAO〉
 *
 * @author 岳雅萱
 * @Date 2019/4/22 16:54
 * @Version 1.0.0
 */
public interface NewsMapper extends BaseMapper<News> {
    @Select("SELECT title,author,publish_time, brief_introduction,link\n" +
            "FROM newsdata;")
    List<News> listNews();
}

package com.xhu.priceanalysis.mapper;

import com.xhu.priceanalysis.pojo.Factor;
import tk.mybatis.mapper.common.Mapper;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 王灏
 * @Date 2019/6/4 9:46
 * @Version 1.0.0
 */
public interface FactorMapper extends Mapper<Factor> {
}

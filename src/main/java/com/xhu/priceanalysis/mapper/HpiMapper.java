package com.xhu.priceanalysis.mapper;

import com.xhu.priceanalysis.pojo.Hpi;
import com.xhu.priceanalysis.vo.EchartsVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 岳雅萱
 * @Date 2019/4/2 9:44
 * @Version 1.0.0
 */
public interface HpiMapper extends BaseMapper<Hpi> {

    @Select("SELECT date name,qoq value1,yoy value2 FROM hpi\n" +
            "WHERE cname = #{name} AND date > DATE_SUB(CURDATE(), INTERVAL 1 YEAR)")
    List<EchartsVO> getYoyAndQoqByCityName(@Param("name") String name);
}

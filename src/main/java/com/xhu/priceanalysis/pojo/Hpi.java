package com.xhu.priceanalysis.pojo;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author
 * @Date 2019/4/1 22:14
 * @Version 1.0.0
 */
@Data
@Table(name = "hpi")
public class Hpi {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Long id;
    private Long cid;    // 城市Id
    private String cname;   // 城市名
    private Date date;      // 日期
    private Double qoq;     // 环比
    private Double yoy;     // 同比
    private Double fixedbase;   // 定基
    private String conclusion;  // 结论
}

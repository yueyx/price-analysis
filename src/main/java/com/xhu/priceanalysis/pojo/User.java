package com.xhu.priceanalysis.pojo;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 用户实体类
 * @Data注解用来自动生成getter和setter以及toString等方法
 */
@Data
@Table(name = "user")   // 与数据中的表绑定
public class User {
    @Id
    private Long uid;   // 用户id
    private String username;    // 用户名
    private String password;    // 密码

}

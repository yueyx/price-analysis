package com.xhu.priceanalysis.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "newsdata")
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class News {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Long id;// id
    private String title; // 标题
    private String author; // 作者
    private String publishTime; // 发布时间
    private String briefIntroduction; // 发布简介
//    private String publishContent; // 发布内容
    private String link;  // 发布链接


}

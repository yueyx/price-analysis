package com.xhu.priceanalysis.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能简述〉
 * 〈城市实体类〉
 *
 * @author
 * @Date 2019/3/30 22:01
 * @Version 1.0.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "city")
public class City {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Long cid;    // 城市id
    private String name;    // 城市名

}

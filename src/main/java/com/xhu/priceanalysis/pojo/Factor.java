package com.xhu.priceanalysis.pojo;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 王灏
 * @Date 2019/6/4 9:43
 * @Version 1.0.0
 */
@Data
@Table(name = "factor")
public class Factor {
    @Id
    private Integer fid;
    private String keyword;
}

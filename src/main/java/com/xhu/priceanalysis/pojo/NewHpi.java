package com.xhu.priceanalysis.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 王灏
 * @Date 2019/5/15 23:04
 * @Version 1.0.0
 */
@Data
@Table(name = "new_hpi")
public class NewHpi {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Long id;
    private Long cid;    // 城市Id
    private String cname;   // 城市名
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date date;      // 日期
    private Integer type;     // 环比
    private Double indexValue;     // 同比
    private Boolean unusualValue;  // 结论
    @Transient
    private List<News> newsList;
}

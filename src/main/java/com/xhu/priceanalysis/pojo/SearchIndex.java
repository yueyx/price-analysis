package com.xhu.priceanalysis.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

/**
 * 〈功能简述〉
 * 〈百度搜索指数实体类〉
 *
 * @author 岳雅萱
 * @Date 2019/5/26 18:37
 * @Version 1.0.0
 */
@Data
@Table(name = "search_index")
public class SearchIndex {
    @Id
    @KeySql(useGeneratedKeys = true)
    private Integer sid;
    private String cityName;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date searchTime;
    private Integer indexValue;
    private Boolean unusualValue;  // 结论

    @Transient
    private List<News> newsList;
}

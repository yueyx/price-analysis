package com.xhu.priceanalysis.vo;

import lombok.Data;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 王灏
 * @Date 2019/5/29 19:25
 * @Version 1.0.0
 */
@Data
public class EchartsVO {
    private String name;
    private Double value1;
    private Double value2;
}

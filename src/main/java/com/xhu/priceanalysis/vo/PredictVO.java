package com.xhu.priceanalysis.vo;

import lombok.Data;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author 王灏
 * @Date 2019/5/30 11:36
 * @Version 1.0.0
 */
@Data
public class PredictVO {
    private Double yoy;
    private Double qoq;
}

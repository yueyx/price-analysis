package com.xhu.priceanalysis.vo;

import lombok.Data;

/**
 * 〈功能简述〉
 * 〈〉
 *
 * @author
 * @Date 2019/6/2 18:42
 * @Version 1.0.0
 */
@Data
public class SearchIndexVO {

    private String cityName;
    private Integer total;
}

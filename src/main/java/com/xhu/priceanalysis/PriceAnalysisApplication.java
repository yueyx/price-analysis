package com.xhu.priceanalysis;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
//@ServletComponentScan
@MapperScan(basePackages = "com.xhu.priceanalysis.mapper")
public class PriceAnalysisApplication {

    public static void main(String[] args) {
        SpringApplication.run(PriceAnalysisApplication.class, args);
    }

//    @RequestMapping("/")
//    public String index(){
//        return "index";
//    }
}

package com.xhu.priceanalysis.util.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 异常枚举类型
 *
 * @Getter 编译的时候自动生成getter方法
 * @NoArgsConstructor 编译的时候自动生成无参构造
 * @AllArgsConstructor 编译的时候自动生成全参构造
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum ExceptionEnum {
    USER_NOT_FOUND(404, "用户不存在!"),
    DELETE_USER_FAILED(500, "删除用户失败!"),
    UPDATE_USER_FAILED(500, "更新用户失败！"),
    SAVE_USER_FAILED(500, "新增用户失败！"),

    SAVE_CITY_FAILED(500,"保存城市失败！"),
    CITY_NOT_FOUND(404, "城市列表为空"),
    NEWS_NOT_FOUND(404,"新闻列表为空"),
    TODAY_NEWS_NOT_FOUND(404, "今日没有新闻"),
    CITY_YOY_AND_QOQ_NOT_FOUND(404, "没有找到该城市的同比和环比指数"),
    SEARCHINDEX_NOT_FOUND(404, "搜索指数未找到");

    private int code;   // HttpStatus状态码
    private String msg;     // 错误消息


}

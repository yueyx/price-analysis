package com.xhu.priceanalysis.util.reptile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xhu.priceanalysis.util.reptile.baiduIndexUtil.collection.CollectionUtil;
import com.xhu.priceanalysis.util.reptile.baiduIndexUtil.datetime.DatetimeUtil;
import com.xhu.priceanalysis.util.reptile.baiduIndexUtil.file.FileUtil;
import com.xhu.priceanalysis.util.reptile.baiduIndexUtil.print.Print;
import com.xhu.priceanalysis.util.reptile.baiduIndexUtil.request.RequestUtil;
import com.xhu.priceanalysis.util.reptile.baiduIndexUtil.spider.BrowserDriverSpiderUtil;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

@Service
public class BaiduIndexService {
    /**
     * 百度指数的首页(的详细)url
     * + 方便后续爬虫在业务流程中的逻辑判断
     */


    private final static String indexUrl = "http://index.baidu.com/v2/index.html";
    private static final Logger logger = Logger.getLogger(BaiduIndexService.class.getName());
    protected static String logPrefix = null;
    /**
     * 存放百度指数(index.baidu.com)网站的Cookie文件
     */
    private final static String baiduIndexCookiesFilePath = "C:\\Users\\岳雅轩\\Desktop\\config\\cookies-baidu-index-yyx.txt";

    /**
     * 注入百度指数网站JS的本地js文件存放路径
     */
    private final static String jsFilePath = "C:\\Users\\岳雅轩\\Desktop\\config\\insert-script.js";
    /**
     * 百度指数网站的用户名
     */
    private final static String username = "18188451772";

    /**
     * 百度指数网站的用户密码
     */
    private final static String password = "yyx19951020";

    /**
     * 模拟登陆
     *
     * @param webDriver 此时传入的webDriver正处于显示了登陆提示框的页面
     * @return WebDriver 已经登陆后的WebDriver(方便进行后续操作)
     * [demo]
     * + 传入的webDriver可能处于这样的url中
     * + http://index.baidu.com/v2/index.html#/?login=1&fromu=http%3A%2F%2Findex.baidu.com%2Fv2%2Fmain%2Findex.html%23%2Ftrend%2F%25E5%258C%2597%25E4%25BA%25AC%25E6%2588%25BF%25E4%25BB%25B7%3Fwords%3D%25E5%258C%2597%25E4%25BA%25AC%25E6%2588%25BF%25E4%25BB%25B7
     * [notice]
     * + 等待是为了避开出现验证码
     */
    public static ChromeDriver simulateLoginByWebDriver(ChromeDriver webDriver) {
        if (webDriver == null) {//must be not null
            return null;
        }
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
        webDriver.findElement(By.cssSelector("#TANGRAM__PSP_4__userName")).sendKeys(username);
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        webDriver.findElement(By.cssSelector("#TANGRAM__PSP_4__password")).sendKeys(password);
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        webDriver.findElement(By.cssSelector("#TANGRAM__PSP_4__submit")).click();
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        webDriver.navigate().refresh();//刷新当前页面(可能还是在首页，也可能是登陆后的页面)
        return webDriver;
    }

    public static ChromeDriver simulateSelectDate(ChromeDriver webDriver,Calendar startDate ,Calendar endDate) {
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
        if (webDriver == null) {//must be not null
            return null;
        }
        webDriver.navigate().refresh();//刷新当前页面(可能还是在首页，也可能是登陆后的页面)
        webDriver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);//隐式等待
        String start=DatetimeUtil.calendarToString(startDate,"yyyy-MM-dd");
        String end=DatetimeUtil.calendarToString(endDate,"yyyy-MM-dd");
        Print.print(start+"start+++++++");
        Print.print(end+"end+++++++");
        String[] startDateArray = start.split("-");
        String[] endDateArray = end.split("-");
        // 选择指定开始年
        webDriver.findElement(By.cssSelector("body > div.index-main > div:nth-child(2) > div.index-main-content > div.index-trend-view > div.index-trend-content.mb30 > div:nth-child(1) > div.tabs.tabs-blue.tabs-line > div.nav.tab-nav-container.border-bottom > div > div.index-date-range-picker > button > span")).click();
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
        webDriver.findElement(By.cssSelector("body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div:nth-child(2) > div > div > div.veui-calendar-head > span.veui-calendar-left > button.veui-calendar-select")).click();
        String year=webDriver.findElement(By.cssSelector("body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-enabled.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right > div > div:nth-child(2) > div > div > div.veui-calendar-head > span.veui-calendar-left > button.veui-calendar-select > b")).getText().trim();
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
        webDriver.findElement(By.cssSelector("#veui-calendar-37\\:panel-title\\:0")).click();
        String month=webDriver.findElement(By.cssSelector("#veui-calendar-37\\:panel-title\\:0 > b")).getText().trim();
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
        if (StringUtils.isBlank(year)||StringUtils.isBlank(month)) {
            System.out.println("year或month是空的"+year+"====="+month);
            return null;
        }
        int currentYear=Integer.valueOf(year);
        int startYear=Integer.valueOf(startDateArray[0]);
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
        if(startYear>currentYear){
            for(int i=0;i<startYear-currentYear;i++) {
                webDriver.findElement(By.cssSelector("body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-enabled.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right > div > div:nth-child(2) > div > div > div.veui-calendar-head > span.veui-calendar-left > button:nth-child(3) > svg")).click();
                webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
            }
        }else {
            for(int i=0;i<currentYear-startYear;i++) {
                webDriver.findElement(By.cssSelector("body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-enabled.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right > div > div:nth-child(2) > div > div > div.veui-calendar-head > span.veui-calendar-left > button:nth-child(1) > svg")).click();
                webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
            }
        }
        // 选择指定开始月
        int currentMonth=Integer.valueOf(month);
        int startMonth=Integer.valueOf(startDateArray[1]);
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
        if(startMonth>currentMonth){
            for(int i=0;i<startMonth-currentMonth;i++) {
                webDriver.findElement(By.cssSelector("body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div:nth-child(2) > div > div > div.veui-calendar-head > span.veui-calendar-right > button:nth-child(3)")).click();
                webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
            }
        }else {
            for(int i=0;i<currentMonth-startMonth;i++) {
                webDriver.findElement(By.cssSelector("body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div:nth-child(2) > div > div > div.veui-calendar-head > span.veui-calendar-right > button:nth-child(1)")).click();
                webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
            }
        }
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        // 选择指定开始日
        int s=0;
        for(int i=1;i<=5;i++){
            if(s==1){
                break;
            }
            for(int j=1;j<=7;j++){
                String str1="body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div:nth-child(2) > div > div > div.veui-calendar-body > table > tbody > tr:nth-child("+Integer.valueOf(i)+") > td:nth-child("+Integer.valueOf(j)+")";
                String str2="body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div:nth-child(2) > div > div > div.veui-calendar-body > table > tbody > tr:nth-child("+Integer.valueOf(i)+") > td:nth-child("+Integer.valueOf(j)+") > button";
                String tempDay=webDriver.findElement(By.cssSelector(str1)).getText().trim();
                String className=webDriver.findElement(By.cssSelector(str1)).getAttribute("class").trim();
                if(Integer.valueOf(tempDay)==Integer.valueOf(startDateArray[2])&&className.equals("veui-calendar-day")){
                    webDriver.findElement(By.cssSelector(str2)).click();
                    s=1;
                    break;
                }
            }
        }
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
        webDriver.findElement(By.cssSelector("body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div.left-wrapper > div:nth-child(3) > div")).click();
        // 选择指定结束年
        webDriver.findElement(By.cssSelector("body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div:nth-child(3) > div > div > div.veui-calendar-head > span.veui-calendar-left > button.veui-calendar-select")).click();
        String year1=webDriver.findElement(By.cssSelector("body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div:nth-child(3) > div > div > div.veui-calendar-head > span.veui-calendar-left > button.veui-calendar-select > b")).getText().trim();
        String sssss=webDriver.findElementByCssSelector("body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div:nth-child(3) > div > div > div.veui-calendar-head > span.veui-calendar-left > button.veui-calendar-select > b").getTagName();
        Print.print(sssss+"ssssssclassName");
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
        webDriver.findElement(By.cssSelector("#veui-calendar-38\\:panel-title\\:0")).click();
        String month1=webDriver.findElement(By.cssSelector("#veui-calendar-38\\:panel-title\\:0 > b")).getText().trim();
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
        if(StringUtils.isBlank(year1)) {
            System.out.println("year1或month1结束是空的"+year1+"------"+month1);
            return null;
        }
        Print.print(endDateArray[0]+"000qqq；+"+endDateArray[1]+"11qqq1;+"+endDateArray[2]+"333qqq;");
        int currentYear1=Integer.valueOf(year1);
        int startYear1=Integer.valueOf(endDateArray[0]);
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
        if(startYear1>currentYear1){
            for(int i=0;i<startYear1-currentYear1;i++) {
                webDriver.findElement(By.cssSelector("body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div:nth-child(3) > div > div > div.veui-calendar-head > span.veui-calendar-left > button:nth-child(3)")).click();
                webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
            }
        }else {
            for(int i=0;i<currentYear1-startYear1;i++) {
                webDriver.findElement(By.cssSelector("body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div:nth-child(3) > div > div > div.veui-calendar-head > span.veui-calendar-left > button:nth-child(1)")).click();
                webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
            }
        }
        // 选择指定结束月
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
        int currentMonth1=Integer.valueOf(month1);
        int startMonth1=Integer.valueOf(endDateArray[1]);
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
        if(startMonth1>currentMonth1){
            for(int i=0;i<startMonth1-currentMonth1;i++) {
                webDriver.findElement(By.cssSelector("body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div:nth-child(3) > div > div > div.veui-calendar-head > span.veui-calendar-right > button:nth-child(3)")).click();
                webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
            }
        }else {
            for(int i=0;i<currentMonth1-startMonth1;i++) {
                webDriver.findElement(By.cssSelector("body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div:nth-child(3) > div > div > div.veui-calendar-head > span.veui-calendar-right > button:nth-child(1)")).click();
                webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
            }
        }
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        // 选择指定结束日
        int s1=0;
        for(int i=1;i<=5;i++){
            if(s1==1){
                break;
            }
            for(int j=1;j<=7;j++){
                String str1="body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div:nth-child(3) > div > div > div.veui-calendar-body > table > tbody > tr:nth-child("+Integer.valueOf(i)+") > td:nth-child("+Integer.valueOf(j)+")";
                String str2="body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div:nth-child(3) > div > div > div.veui-calendar-body > table > tbody > tr:nth-child("+Integer.valueOf(i)+") > td:nth-child("+Integer.valueOf(j)+") > button";
                String tempDay=webDriver.findElement(By.cssSelector(str1)).getText().trim();
                String className=webDriver.findElement(By.cssSelector(str1)).getAttribute("class").trim();
                Print.print("classNAme------"+className+"------classNAme");
                Print.print("Integer.valueOf(tempDay)==Integer.valueOf(endDateArray[2]"+Integer.valueOf(tempDay)+"---"+Integer.valueOf(endDateArray[2]));
                if(Integer.valueOf(tempDay)==Integer.valueOf(endDateArray[2]) && className.equals("veui-calendar-day")){
                    webDriver.findElement(By.cssSelector(str2)).click();
                    s1=1;
                    break;
                }
            }
        }
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
        // 点击确定按钮
        webDriver.findElement(By.cssSelector("body > div.veui-overlay-box.index-date-range-picker-overlay-box.tether-element.tether-element-attached-top.tether-element-attached-right.tether-target-attached-bottom.tether-target-attached-right.tether-enabled > div > div.left-wrapper > div.button-wrapper > div > span.primary")).click();
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
        return webDriver;
    }
    /**
     * 通过Web浏览器驱动加载待检索关键词的百度指数页面
     *
     * @param query 查询的关键词
     * @return webDriver  此时可直接操纵它来获取/操纵想要的网页数据或者行为
     * <p>
     * [dependency] 本函数处理过程中，依赖于开发者的其他一些类
     * + BrowserDriverSpiderUtil
     * + getChromeDriver()
     * + RequestUtil
     * + urlEncode()
     * + getCookiesFromLocalFile()
     * + addCookiesForWebDriver()
     * + CollectionUtil
     * + collectionToList
     */
    public static ChromeDriver loadBaiduIndexPageByWebDriver(String query) {
        //step1 生成目标查询链接
        String queryMainUrl = "http://index.baidu.com/v2/main/index.html#/trend/" + RequestUtil.urlEncode(query) + "?words=" + RequestUtil.urlEncode(query);//queryMainUrl：百度指数查询后的(结果)主页
        //step2 获取已经过初始化的 WebDriver (默认：ChromeDriver)
        ChromeDriver webDriver = BrowserDriverSpiderUtil.getChromeDriver(false);//isHeadless：false 关闭无头模式：查看运行过程，方便调试
        //step3 直接请求queryMainUrl链接
        webDriver.get(queryMainUrl);
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
        //step4 添加Cookie到webDriver中
        List<Cookie> cookies = null;
        cookies = CollectionUtil.collectionToList(RequestUtil.getCookiesFromLocalFile(true, baiduIndexCookiesFilePath).values());
        RequestUtil.addCookiesForWebDriver(webDriver, cookies);
        String currentUrl = webDriver.getCurrentUrl();//获取当前webDriver所处的url
        System.out.println("now url:\n" + currentUrl);//just for test
        //step5 判断当前是否处于百度指数的结果页
        if (currentUrl.startsWith(indexUrl)) {//当前页面是首页Url（即 要求登录）
            //step5.1 模拟登陆
            webDriver = simulateLoginByWebDriver(webDriver);
            currentUrl = webDriver.getCurrentUrl();
            if (currentUrl.startsWith(indexUrl)) {//如果当前页面还是首页(即 虽然已经登陆成功，但并没有处于爬虫期待的 queryMainUrl 的数据页面)
                //方式一 再次【重新】请求 queryMainUrl
                webDriver.get(queryMainUrl);
                webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//隐式等待
                System.out.println("[#loadBaiduIndexPageByWebDriver] now url:\n" + currentUrl);//just for test
                //方式二 在当前所处的百度指数首页中模拟输入我们要查询的关键词，将会跳转到 queryMainUrl 页面
            }
        } else {//第一次请求便已成功获取到百度指数页(queryMainUrl)
        }
        System.out.println("now url:\n" + currentUrl);//just for test
//        System.out.print(webDriver.findElement(By.cssSelector("html")).getText());//just for test
//        webDriver.quit();//退出webDriver浏览器
        webDriver.navigate().refresh();//刷新一次
        return webDriver;
    }
    /**
     * 获取关键词为"QueryWord"的百度指数的开始日期和结束日期
     *
     * @param webDriver 查询百度指数后的(首个)初始结果页面的 webDriver
     * @return Calendar[]  日期数组，第1个为开始日期，第2个为结束日期
     */
    public static List<BaiduIndex> resolveBaiduIndexValues(String query) {
        List<BaiduIndex> baiduIndexs = new ArrayList<BaiduIndex>();
        //step 1 加载百度指数的查询数据页
        ChromeDriver webDriver = (ChromeDriver) BaiduIndexService.loadBaiduIndexPageByWebDriver(query);//向下转型回 ChromeDriver,否则WebDriver没有执行JS脚本的方法
        webDriver.manage().window().maximize();       //将浏览器窗口最大化
        //step 2 得到获取数据的起止日期
        Calendar[] calendars = new Calendar[2];
        String startAndEndDatetimeStr = null;//形如："2011-01-01 ~ 2019-05-15"
        String[] startAndEndDatetimeStrArray = null;
        webDriver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
        webDriver.findElement(By.cssSelector("body > div.index-main > div:nth-child(2) > div.index-main-content > div.index-trend-view > div.index-trend-content.mb30 > div:nth-child(1) > div.tabs.tabs-blue.tabs-line > div.nav.tab-nav-container.border-bottom > div > div:nth-child(3) > button > span:nth-child(1)")).click();//点击 "全部"菜单按钮
        webDriver.findElement(By.cssSelector("body > div.veui-overlay-box.index-dropdown-list-overlay-box.tether-element.tether-abutted.tether-abutted-left.tether-element-attached-top.tether-element-attached-left.tether-target-attached-bottom.tether-target-attached-left.tether-enabled > div > div > div:nth-child(6)")).click(); //点击 "全部" 按钮(与上面的 全部 不同)
        webDriver.findElement(By.cssSelector("body > div.index-main > div:nth-child(2) > div.index-main-content > div.index-trend-view > div.index-trend-content.mb30 > div:nth-child(1) > div.tabs.tabs-blue.tabs-line > div.nav.tab-nav-container.border-bottom > div > div.index-date-range-picker > button > span")).click();
        webDriver.manage().timeouts().implicitlyWait(50000, TimeUnit.MILLISECONDS);
        startAndEndDatetimeStr = webDriver.findElement(By.cssSelector("body > div.index-main > div:nth-child(2) > div.index-main-content > div.index-trend-view > div.index-trend-content.mb30 > div:nth-child(1) > div.tabs.tabs-blue.tabs-line > div.nav.tab-nav-container.border-bottom > div > div.index-date-range-picker > button > span")).getText().trim();
        webDriver.manage().timeouts().implicitlyWait(50000, TimeUnit.MILLISECONDS);
        if (startAndEndDatetimeStr == null) {
            logger.warning(logPrefix + "Fail to resolve startAndEndDatetimeStr from html!");
            return baiduIndexs;
        }
        logger.info(logPrefix + "startAndEndDatetimeStr:" + startAndEndDatetimeStr);//just for test
        startAndEndDatetimeStrArray = startAndEndDatetimeStr.split("~");
        Print.print(startAndEndDatetimeStrArray[0]+"=========="+startAndEndDatetimeStrArray[1]);
        try {
            calendars[0] = DatetimeUtil.stringToCalendar(startAndEndDatetimeStrArray[0].trim(), "yyyy-MM-dd");
            calendars[1] = DatetimeUtil.stringToCalendar(startAndEndDatetimeStrArray[1].trim(), "yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(calendars==null){
            logger.warning(logPrefix+"Fail to resolve startDatime and endDatetime !");
            return baiduIndexs;//size:0
        }
        int step = 365;
        int allDaySize=0;//总共的天数
        SimpleDateFormat myFormatter = new SimpleDateFormat( "yyyy-MM-dd");
        try {
            java.util.Date endDateTemp = myFormatter.parse( startAndEndDatetimeStrArray[1]);
            java.util.Date startDateTemp= myFormatter.parse( startAndEndDatetimeStrArray[0]);
            allDaySize=(int)((endDateTemp.getTime()-startDateTemp.getTime())/(24*60*60*1000));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar startDate = calendars[0]; //开始日期初始化
        Calendar endDate = null;
        for(int i=0;i<allDaySize;i=i+step){
            String temp=DatetimeUtil.calendarToString(startDate,"yyyy-MM-dd");
            Calendar tempStartDate=null;
            endDate = startDate;
            endDate.add(Calendar.DATE,step);
            try {
                tempStartDate=DatetimeUtil.stringToCalendar(temp,"yyyy-MM-dd");
                Print.print(temp+"tempStartDate");
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if(endDate.getTimeInMillis()>calendars[1].getTimeInMillis()){ //判断时间戳大小
                endDate = Calendar.getInstance(calendars[1].getTimeZone());
            }

            // step 3 设置起止日期
            webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
            webDriver = (ChromeDriver) BaiduIndexService.simulateSelectDate(webDriver,tempStartDate,endDate);
            webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
            if (webDriver == null) {
                webDriver.close();
                webDriver = (ChromeDriver) BaiduIndexService.loadBaiduIndexPageByWebDriver(query);//向下转型回 ChromeDriver,否则WebDriver没有执行JS脚本的方法
                webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
                webDriver = (ChromeDriver) BaiduIndexService.simulateSelectDate(webDriver,tempStartDate,endDate);
            }
            webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
            String jsCode = FileUtil.readFile(jsFilePath);
            try {
                webDriver.executeScript(jsCode);
                //step 3 插入解析百度指数的[js文件]脚本入百度指数网页中 RequestUtil.getJavaScriptEngine();

                //为什么是插入js文件，而不是直接执行js脚本？因为：通过 webDriver 可执行的部分 js 函数无法正常执行，故而采取此下策
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println("[BaiduIndex.resolveBaiduIndexValues] Fail to execute javascript code!");
                return baiduIndexs;//size:0
            }
            ObjectMapper mapper = new ObjectMapper();//jackson
            String baiduIndexsJsonStr = null;
            //step 4 等待鼠标模拟滑动js脚本执行完毕
            webDriver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);//隐式等待
            //step 5 获取脚本采集好的json数据(格式：百度指数json数组)

            try {
                baiduIndexsJsonStr = webDriver.findElement(By.cssSelector("#webDriver_housePrices")).getText().trim();//获取js脚本产生的json字符串
                if (baiduIndexsJsonStr != null) {
                    //step 6 通过jackson将json字符串转为 Java (数组)对象
                    baiduIndexs.addAll(mapper.readValue(baiduIndexsJsonStr, new TypeReference<List<BaiduIndex>>() {}));
                } else {
                    System.err.println("[BaiduIndex.resolveBaiduIndexValues] baiduIndexsJsonStr is null!");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);//隐式等待
            startDate = endDate;
            startDate.add(Calendar.DATE,1);
            Print.print(DatetimeUtil.calendarToString(startDate,"yyyy-MM-dd")+"startDatea=====");
        }
        return baiduIndexs;
    }
    public List<BaiduIndex> reptileBaiduIndex() {
        String query = "成都房价";
        return BaiduIndexService.resolveBaiduIndexValues(query);
    }

    public static void main(String[] args) {
        String query = "成都房价";
        List<BaiduIndex> baiduIndexList = BaiduIndexService.resolveBaiduIndexValues(query);
        Print.print(baiduIndexList);//获取解析的数据(全部)
    }
}

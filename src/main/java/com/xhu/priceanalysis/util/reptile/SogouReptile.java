package com.xhu.priceanalysis.util.reptile;

import com.xhu.priceanalysis.pojo.News;
import com.xhu.priceanalysis.util.reptile.baiduIndexUtil.spider.BrowserDriverSpiderUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SogouReptile {
    public static String getText(String url) {  // 获取新闻的所有内容
        try {
            String text=null;
            Document document = Jsoup.connect(url).get();
            if(document!=null){
                return "1";
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }
        return null;
    }

    public static int getTimeNumber(String time) {
        String str = "";
        time.trim();
        if (time != null && !"".equals(time)) {
            for (int i = 0; i < time.length(); i++) {
                if (time.charAt(i) >= 48 && time.charAt(i) <= 57) {
                    str = str + time.charAt(i);
                }
            }
        }
        return Integer.parseInt(str);
    }

    public static String getTimeByMinute(int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, minute);
        return new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
    }

    public static String getTimeByHour(int hour) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + hour);
        return new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
    }

    /**
     * 爬取新闻信息
     *
     * @return 新闻信息列表
     */
    public static List<News> reptileNews() {
        ArrayList<News> news = new ArrayList<News>();
        //获取整个网站的根节点，也就是html开头部分一直到结束
        String queryMainUrl = "https://news.sogou.com/news?mode=1&manual=&query=%B7%BF%BC%DB&time=0&sort=0&page=1&w=03009900&dr=1%22;";//queryMainUrl：百度指数查询后的(结果)主页
        //step2 获取已经过初始化的 WebDriver (默认：ChromeDriver)
        ChromeDriver webDriver = BrowserDriverSpiderUtil.getChromeDriver(false);//isHeadless：false 关闭无头模式：查看运行过程，方便调试
        //step3 直接请求queryMainUrl链接
        webDriver.get(queryMainUrl);
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
        webDriver.manage().window().maximize();       //将浏览器窗口最大化
        String nextPage = "1";
        while (nextPage!=null){
            for (int i = 0; ; i++) {
                String title = null;
                try {
                    title = webDriver.findElement(By.cssSelector("#uigs_" + Integer.valueOf(i))).getText().trim();
                } catch (Exception e) {
                    System.out.println("为空");
                    break;
                }
                webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
                String text = webDriver.findElement(By.cssSelector("#summary_" + Integer.valueOf(i))).getText().trim();
                webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
                String link = webDriver.findElement(By.cssSelector("#uigs_" + Integer.valueOf(i))).getAttribute("href");
                webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
                String tempTime = webDriver.findElement(By.cssSelector("#main > div > div:nth-child(" + Integer.valueOf(i + 1) + ") > div > div > div > p.news-from")).getText().trim();
                webDriver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
                String[] arr = tempTime.split("\\s+");
                String author = arr[0];
                String time = arr[1];
                if (time.indexOf("小时") != -1) {
                    int num = getTimeNumber(time);
                    time = getTimeByHour(num);
                } else if (time.indexOf("分钟") != -1) {
                    int num = getTimeNumber(time);
                    time = getTimeByMinute(num);
                }
                webDriver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);//隐式等待
                String getTextResult = getText(link);
                if (getTextResult != null) {  // 去掉网页不存在或为空的网页
                    News newdate = new News(null, title, author, time, text, link);
                    System.out.println("新闻==" + newdate);
                    news.add(newdate);
                }
                webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
            }
            webDriver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);//隐式等待
            try {
                nextPage = webDriver.findElement(By.cssSelector("#sogou_next")).getText().trim();
                webDriver.manage().timeouts().implicitlyWait(8000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
                webDriver.findElement(By.cssSelector("#sogou_next")).click();
                webDriver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
            } catch (Exception e) {
                System.out.println("没有下一页");
                nextPage=null;
            }
        }
        return news;
    }

    public static void main(String[] args) {
        SogouReptile.reptileNews();
//            String url = "https://news.sogou.com/news?mode=1&manual=&query=%B7%BF%BC%DB&time=0&sort=0&page=1&w=03009900&dr=1";
//            int index = 1;
//            Document document = Jsoup.connect(url).get();
//            Elements result = document.select("#main > div > div");
//            while (result != null) {
//                for (Element element : result) {
//                    if (element == result.last()) {  // 除去每页最后一个不符合的数据
//                        break;
//                    }
//                    String title = element.select("div > h3 >a").text();
//                    String text = element.select("div > div > div > p.news-txt").text();
//                    String temp = element.select("div > div > div > p.news-from").text();
//                    String[] arr = temp.split("\\s+");
//                    String author = arr[0];
//                    String time = arr[1];
//                    if (time.indexOf("小时") != -1) {
//                        int num = getTimeNumber(time);
//                        time = getTimeByHour(num);
//                    } else if (time.indexOf("分钟") != -1) {
//                        int num = getTimeNumber(time);
//                        time = getTimeByMinute(num);
//                    }
//                    String href = element.select("div > h3 >a").attr("href");
//                    String getTextResult=getText(href);
//                    if(getTextResult==null){  // 去掉网页不存在或为空的网页
//                        break;
//                    }
////                    String publishContent = getText(href);
//                    System.out.println("href" + href);
//                    News newdate = new News(null, title, author, time, text, href);
//                    news.add(newdate);
//                }
//                index = index + 1;
//                url = "https://news.sogou.com/news?mode=1&manual=&query=%B7%BF%BC%DB&time=0&sort=0&page=" + index + "&w=03009900&dr=1";
//                document = Jsoup.connect(url).get();
//                result = document.select("#main > div > div");
//            }
//            System.out.println(news);
    }



}

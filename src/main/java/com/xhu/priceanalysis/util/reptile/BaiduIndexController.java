package com.xhu.priceanalysis.util.reptile;

import com.xhu.priceanalysis.util.reptile.baiduIndexUtil.file.FileUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Logger;
@Controller
public class BaiduIndexController {
    private static final Logger logger = Logger.getLogger(BaiduIndexController.class.getName());
    //日志前缀字符串,方便通过日志定位程序
    protected String logPrefix = null;
    /**
     * resolveBaiduIndexByJs.js 获取百度指数解析js脚本
     **/
    @RequestMapping("/baiduIndex/resolveBaiduIndexByJs.js")
    @ResponseBody
    public String resolveBaiduIndexByJs(HttpServletRequest request, HttpServletResponse response){
        logPrefix = "[BaiduSpiderController.login] ";
        String resolveBaiduIndexByJsPath = "C:\\Users\\岳雅轩\\Desktop\\config\\resolveBaiduIndexByJs.js";
        String jsCodeText = null;
        jsCodeText = FileUtil.readFile(resolveBaiduIndexByJsPath);
        if(jsCodeText==null){
            return "not found the js file!";
        } else {
            return jsCodeText;
        }
    }
}
package com.xhu.priceanalysis.util.reptile;

import lombok.Data;

import java.util.Calendar;

/**
 * @IDE: Created by IntelliJ IDEA.
 * @Author: 岳雅萱
 * @Date: 2019/5/15  15:59:11
 * @Description: 房价
 */

@Data
public class BaiduIndex {
    /**
     * 关键词
     */
    private String keyword;

    /**
     * 日期
     */
    private Calendar date;

    /**
     * 百度指数值
     */
    private Integer indexValue;
    private Boolean unusualValue;  // 结论

}

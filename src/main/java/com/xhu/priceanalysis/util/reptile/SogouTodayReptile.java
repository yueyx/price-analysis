package com.xhu.priceanalysis.util.reptile;
import com.xhu.priceanalysis.pojo.News;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.util.StringUtils;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
public class SogouTodayReptile {
    public static int getTimeNumber(String time) {
        String str = "";
        time.trim();
        if (time != null && !"".equals(time)) {
            for (int i = 0; i < time.length(); i++) {
                if (time.charAt(i) >= 48 && time.charAt(i) <= 57) {
                    str = str + time.charAt(i);
//                        System.out.println("number+"+str);
                }
            }
        }
        return Integer.parseInt(str);
    }

    public static String getTimeByMinute(int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, minute);
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
    }

    public static String getTimeByHour(int hour) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + hour);
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
    }

    /**
     * 爬取新闻信息
     *
     * @return 新闻信息列表
     */
    public static List<News> reptileNews(Elements result) {
        ArrayList<News> news = new ArrayList<News>();
        try {
            for (Element element : result) {
                if (element == result.last()) {  // 除去每页最后一个不符合的数据
                    break;
                }
                String title = element.select("div > h3 >a").text();
                String text = element.select("div > div > div > p.news-txt").text();
                String temp = element.select("div > div > div > p.news-from").text();
                String[] arr = temp.split("\\s+");
                String author = arr[0];
                String time = arr[1];
                if (time.indexOf("小时") != -1) {
                    int num = getTimeNumber(time);
                    time = getTimeByHour(num);
                } else if (time.indexOf("分钟") != -1) {
                    int num = getTimeNumber(time);
                    time = getTimeByMinute(num);
                }

                String href = element.select("div > h3 >a").attr("href");
                System.out.println("href" + href);
                if (!StringUtils.isEmpty(title)) {
                    News newdate = new News(null, title, author, time, text, href);
                    if (newdate != null) {
                        news.add(newdate);
                    }
                }
            }
        } catch (Exception e) {
        }
        return news;
    }
    public static List<News> getTodayNews() {
        ArrayList<News> news = new ArrayList<News>();
        try {
            //获取整个网站的根节点，也就是html开头部分一直到结
            String url = "https://news.sogou.com/news?mode=1&manual=&query=%B7%BF%BC%DB&time=0&sort=0&page=1&w=03009900&dr=1";
            Document document = Jsoup.connect(url).get();
            Elements result = document.select("#main > div > div");
            for (Element element : result) {
                if (element == result.last()) {  // 除去每页最后一个不符合的数据
                    break;
                }
                String title = element.select("div > h3 >a").text();
                String text = element.select("div > div > div > p.news-txt").text();
                String temp = element.select("div > div > div > p.news-from").text();
                String[] arr = temp.split("\\s+");
                String author = arr[0];
                String time = arr[1];
                if (time.indexOf("小时") != -1) {
                    int num = getTimeNumber(time);
                    time = getTimeByHour(num);
                } else if (time.indexOf("分钟") != -1) {
                    int num = getTimeNumber(time);
                    time = getTimeByMinute(num);
                }

                String href = element.select("div > h3 >a").attr("href");
                System.out.println("href" + href);
                News newdate = new News(null, title, author, time, text, href);
                news.add(newdate);
            }
//            System.out.println(news);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return news;
    }
    public static void main(String[] args) {
        ArrayList<News> news = new ArrayList<News>();
        try {
            //获取整个网站的根节点，也就是html开头部分一直到结
            String url = "https://news.sogou.com/news?mode=1&manual=&query=%B7%BF%BC%DB&time=0&sort=0&page=1&w=03009900&dr=1";
            Document document = Jsoup.connect(url).get();
            Elements result = document.select("#main > div > div");
                for (Element element : result) {
                    if (element == result.last()) {  // 除去每页最后一个不符合的数据
                        break;
                    }
                    String title = element.select("div > h3 >a").text();
                    String text = element.select("div > div > div > p.news-txt").text();
                    String temp = element.select("div > div > div > p.news-from").text();
                    String[] arr = temp.split("\\s+");
                    String author = arr[0];
                    String time = arr[1];
                    if (time.indexOf("小时") != -1) {
                        int num = getTimeNumber(time);
                        time = getTimeByHour(num);
                    } else if (time.indexOf("分钟") != -1) {
                        int num = getTimeNumber(time);
                        time = getTimeByMinute(num);
                    }

                    String href = element.select("div > h3 >a").attr("href");
                    System.out.println("href" + href);
                    News newdate = new News(null, title, author, time, text, href);
                    news.add(newdate);
                }
            System.out.println(news);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(news);
    }


}

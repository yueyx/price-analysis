package com.xhu.priceanalysis.util.reptile.baiduIndexUtil.spider;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class BrowserDriverSpiderUtil {
    private static String systemPropertyNameOfBrowser="webdriver.chrome.driver";

    private static String systemPropertyValueOfBrowser="C:\\\\Users\\\\岳雅轩\\\\AppData\\\\Local\\\\Google\\\\Chrome\\\\Application\\\\chromedriver.exe";

    static {
        System.setProperty(systemPropertyNameOfBrowser,systemPropertyValueOfBrowser);
    }

    public synchronized static ChromeDriver getChromeDriver(){
        return getChromeDriver(true);//默认：开启无头模式
    }

    public final static String userAgent(){
        return "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36";
    }

    /**
     * @param isHeadless 是否开启无头模式
     *    + true  开启无头模式
     *    + false 关闭无头模式
     */
    public synchronized static ChromeDriver getChromeDriver(boolean isHeadless){
        ChromeOptions chromeOptions;
        chromeOptions = new ChromeOptions();

        if(isHeadless){
            chromeOptions.setHeadless(true);//开启无头模式 方式一
//            chromeOptions.addArguments("--headless");//无头模式 方式二
        }
//        chromeOptions.addArguments("--disable-images");//禁用图像[测试无效] 其他："--disable-plugins","--start-maximized","--disable-javascript"
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.managed_default_content_settings.images", 2);//禁用图像[测试有效]
        chromeOptions.addArguments("--http_proxy=118.18.188.188");
        chromeOptions.addArguments("--user-agent="+userAgent());
        ChromeDriver chromeDriver = new ChromeDriver(chromeOptions);
        chromeDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);//隐式等待15秒
        return chromeDriver;
    }

    public static String getSystemPropertyNameOfBrowser() {
        return systemPropertyNameOfBrowser;
    }

    public static void setSystemPropertyNameOfBrowser(String systemPropertyNameOfBrowser) {
        BrowserDriverSpiderUtil.systemPropertyNameOfBrowser = systemPropertyNameOfBrowser;
    }

    public static String getSystemPropertyValueOfBrowser() {
        return systemPropertyValueOfBrowser;
    }

    public static void setSystemPropertyValueOfBrowser(String systemPropertyValueOfBrowser) {
        BrowserDriverSpiderUtil.systemPropertyValueOfBrowser = systemPropertyValueOfBrowser;
    }
}

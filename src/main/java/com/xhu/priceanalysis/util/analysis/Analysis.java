package com.xhu.priceanalysis.util.analysis;

import com.xhu.priceanalysis.util.reptile.baiduIndexUtil.collection.CollectionUtil;
import com.xhu.priceanalysis.util.reptile.baiduIndexUtil.datetime.DatetimeUtil;
import com.xhu.priceanalysis.util.reptile.baiduIndexUtil.request.RequestUtil;
import com.xhu.priceanalysis.util.reptile.baiduIndexUtil.spider.BrowserDriverSpiderUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.chrome.ChromeDriver;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Analysis {
    private final static String baiduAnalysisCookiesFilePath = "C:\\Users\\岳雅轩\\Desktop\\config\\cookies-baidu-analysis-yyx.txt";
    private final static String baiduAnalysisCookiesPosFilePath="C:\\Users\\岳雅轩\\Desktop\\config\\cookies-baidu-analysis-yyx.txt";
    private final static String baiduAnalysisCookiesEclickFilePath="C:\\Users\\岳雅轩\\Desktop\\config\\cookies-baidu-analysis-yyx.txt";
    public static List<AnalysisReptileList> resolveBaiduByFactor(String city, String factor, Calendar cal){
        ArrayList<AnalysisReptileList> analysisList=new ArrayList<AnalysisReptileList>();
        String queryMainUrl = "https://www.baidu.com/";//queryMainUrl：百度指数查询后的(结果)主页
        //step2 获取已经过初始化的 WebDriver (默认：ChromeDriver)
        ChromeDriver webDriver = BrowserDriverSpiderUtil.getChromeDriver(false);//isHeadless：false 关闭无头模式：查看运行过程，方便调试
        //step3 直接请求queryMainUrl链接
        webDriver.get(queryMainUrl);
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);//隐式等待
        webDriver.manage().window().maximize();       //将浏览器窗口最大化
        webDriver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
        webDriver.findElement(By.cssSelector("#kw")).sendKeys(city+" "+factor);
//        webDriver.navigate().refresh();//刷新当前页面(可能还是在首页，也可能是登陆后的页面)
        webDriver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
        webDriver.findElement(By.cssSelector("#su")).click();
        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);//隐式等待
        List<Cookie> cookiesBaidu = null;
        List<Cookie> cookiesPos= null;
        List<Cookie> cookiesEclick= null;
        String pos="https://pos.baidu.com";
        String eclick="https://eclick.baidu.com";
        cookiesBaidu = CollectionUtil.collectionToList(RequestUtil.getCookiesFromLocalFile(true, baiduAnalysisCookiesFilePath).values());
        cookiesPos = CollectionUtil.collectionToList(RequestUtil.getCookiesFromLocalFile(true, baiduAnalysisCookiesPosFilePath).values());
        cookiesEclick = CollectionUtil.collectionToList(RequestUtil.getCookiesFromLocalFile(true, baiduAnalysisCookiesEclickFilePath).values());
        RequestUtil.addCookiesForWebDriver(webDriver,cookiesPos);
        RequestUtil.addCookiesForWebDriver(webDriver, cookiesBaidu);
        RequestUtil.addCookiesForWebDriver(webDriver, cookiesEclick);
        webDriver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
        webDriver.findElement(By.cssSelector("#s_tab > div > a:nth-child(2)")).click();
        webDriver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);//启动请求后，隐式等待 1s 模拟真实情况，以防止被网站察觉是爬虫
        webDriver.findElement(By.cssSelector("#\\31  > div > div.c-span18.c-span-last > p")).getText().trim();
        String isImgText="div.c-span18.c-span-last > p";
        String noimg="p";
        String img="div.c-span6 > a > img";
        int totle=0;
        for(int i=1;i<10;i++){
            String str="#\\3"+Integer.valueOf(i)+"  > div > ";
            String text=null;
            if(webDriver.findElement(By.cssSelector(str+img)).getAttribute("class").trim()=="c_photo"){
                text=webDriver.findElement(By.cssSelector(str+isImgText)).getText().trim();
            }else{
                text=webDriver.findElement(By.cssSelector(str+noimg)).getText().trim();
            }
            String[] siteDateArray=text.split("  ");
            Calendar calendar = Calendar.getInstance();
            try {
                calendar=DatetimeUtil.stringToCalendar(siteDateArray[1].trim(), "yyyy-MM-dd");
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String date1=DatetimeUtil.calendarToString(cal,"yyyy-MM-dd");
            String date2=DatetimeUtil.calendarToString(calendar,"yyyy-MM-dd");
            String[] date1Arrays=date1.split("-");
            String[] date2Arrays=date1.split("-");
            if(Integer.valueOf(date1Arrays[1])==Integer.valueOf(date2Arrays[1])){
                totle=totle+1;
            }
        }
//        AnalysisReptileList st=new AnalysisReptileList(null,city,factor,cal,totle);
//        analysisList.add(st);
        return analysisList;
    }
    public static void main(String[] args) {
        Calendar dateTest = null;
        try {
            dateTest = DatetimeUtil.stringToCalendar("2019-01-01","yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Analysis.resolveBaiduByFactor("北京","地区生产总值",dateTest);
    }
}

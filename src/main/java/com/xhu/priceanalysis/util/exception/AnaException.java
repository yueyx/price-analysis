package com.xhu.priceanalysis.util.exception;

import com.xhu.priceanalysis.util.enums.ExceptionEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 自定义异常类
 */

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AnaException extends RuntimeException {

    private ExceptionEnum exceptionEnums;

}
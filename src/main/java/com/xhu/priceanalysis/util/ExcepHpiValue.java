package com.xhu.priceanalysis.util;


import com.xhu.priceanalysis.mapper.NewHpiMapper;
import com.xhu.priceanalysis.pojo.NewHpi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

@Component
public class ExcepHpiValue {

    private static int length = 0;
    @Autowired
    private NewHpiMapper newHpiMapper;

    public void calExcepValue(List<NewHpi> newHpis) {
        length = newHpis.size();
        calc(newHpis);
    }
    public  void calc(List<NewHpi> dataArrayList) {
        //因为格拉布斯准则只能对大于等于3个数据进行判断，所以数据量小于3时，直接返回
        if (dataArrayList.size() < 3) {
            return;
        }
        //求出数据平均值和标准差
        List<Double> collect = dataArrayList.stream().map(data -> data.getIndexValue()).collect(Collectors.toList());
        double average = calcAverage(collect);
        double standard = calcStandard(collect, length, average);
        // 循环取每个数据和平均数据的标准差，过了就剔除！
        ListIterator<NewHpi> it = dataArrayList.listIterator();
        while (it.hasNext()) {

            NewHpi item = it.next();
            double value = item.getIndexValue();
            NewHpi previous = it.previous();
            NewHpi next = it.next();

            if (previous != null) {
                if (next != null) {
                    value = (previous.getIndexValue()+item.getIndexValue()+next.getIndexValue())/3;
                } else {
                    value = (previous.getIndexValue()+item.getIndexValue())/2;
                }
            } else {
                if (next != null) {
                    value = (item.getIndexValue()+next.getIndexValue())/2;
                }
            }
            if (value < average - 2*standard || value > average + 2*standard) {
                item.setUnusualValue(true);
                newHpiMapper.updateByPrimaryKeySelective(item);
            }
        }
    }

    //求平均
    public  double calcAverage(List<Double> sample) {
        double sum = 0;
        int cnt = 0;
        for (int i = 0; i < sample.size(); i++) {
            sum += sample.get(i);
            cnt++;
        }
        return (double) sum / cnt;
    }

    //求标准差
    private  double calcStandard(List<Double> array, int n, double average) {
        double sum = 0;
        for (int i = 0; i < n; i++) {
            sum += ((double) array.get(i) - average)
                    * ((double) array.get(i) - average);
        }
        return (double) Math.sqrt((sum / (n - 1)));
    }

}
